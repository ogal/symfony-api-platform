<?php


namespace App\Service\User;


use App\Entity\User;
use App\Exception\User\UserAlreadyExists;
use App\Repository\UserRepository;
use App\Service\Password\EncoderService;
use App\Service\Request\RequestService;
use Symfony\Component\HttpFoundation\Request;

class UserRegisterService
{
    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;
    /**
     * @var EncoderService
     */
    private EncoderService $encoderService;

    /**
     * UserRegisterService constructor.
     */
    public function __construct(UserRepository $userRepository, EncoderService $encoderService)
    {
        $this->userRepository = $userRepository;
        $this->encoderService = $encoderService;
    }

    public function create(Request $request): User
    {
        $name = RequestService::getField($request, 'name');
        $email = RequestService::getField($request, 'email');
        $password = RequestService::getField($request, 'password');

        $user = new User($name, $email);
        /** PRINC.: Tell don't ask. Le pedimos generar passwd, si falla ya nos dará excepción */
        $user->setPassword($this->encoderService->generateEncodedPassword($user, $password));

        try {
            $this->userRepository->save($user);
        } catch (\Exception $exception) {
            throw UserAlreadyExists::fromEmail($email);
        }
        return $user;
    }
}