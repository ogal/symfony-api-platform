<?php


namespace App\Service\Password;


use App\Entity\User;
use App\Exception\Password\PasswordException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service_locator;

class EncoderService
{
    private const MINIMUM_LENGTH = 6;

    /**
     * @var UserPasswordEncoderInterface
     * Interface de Symfony para cifrar y recuperar contraseñas
     */
    private UserPasswordEncoderInterface $userPasswordEncoder;

    /**
     * EncoderService constructor.
     */
    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    public function generateEncodedPassword(UserInterface $user, string $password)
    {
        if (self::MINIMUM_LENGTH > strlen($password)) {
            throw PasswordException::invalidLength();
        } else {
            return $this->userPasswordEncoder->encodePassword($user, $password);
        }
    }

    public function isValidPassword(User $user, string $oldPassword): bool
    {
        return $this->userPasswordEncoder->isPasswordValid($user, $oldPassword);
    }
}