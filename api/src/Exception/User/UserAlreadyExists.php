<?php


namespace App\Exception\User;


use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

/** Lanzará un código Http 409 que será captado por nuestro Listener */
class UserAlreadyExists extends ConflictHttpException
{
    private const MESSAGE = 'User with email %s already exists';

    public static function fromEmail(string $email): self
    {
        throw new self(sprintf(self::MESSAGE, $email));
    }
}