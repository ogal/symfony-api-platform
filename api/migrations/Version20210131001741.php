<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210131001741 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Crea la tabla `user`';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE user (
                                id VARCHAR(255) NOT NULL, 
                                name VARCHAR(255) NOT NULL, 
                                email VARCHAR(255) NOT NULL, 
                                password VARCHAR(255) DEFAULT NULL, 
                                avatar VARCHAR(255) DEFAULT NULL, 
                                token VARCHAR(255) DEFAULT NULL, 
                                is_active TINYINT(1) NOT NULL, 
                                reset_password_token VARCHAR(255) DEFAULT NULL, 
                                created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
                                updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
                                INDEX IDX_user_email (email),
                                CONSTRAINT U_user_email UNIQUE KEY (email)
                                ) 
                                DEFAULT CHARACTER SET utf8mb4 
                                COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB
                     ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE user');
    }
}
